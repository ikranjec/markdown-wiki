# Common Headers

This section describes a handful of headers, which we found raised the most questions in our daily usage, or which are useful in particular circumstances but not widely known.

<br>

## <strong><span style="color:red">Must: </span>Use Content Headers Correctly</strong>

<br>

Content or entity headers are headers with a <code>Content-</code> prefix. They describe the content of the body of the message and they can be used in both, HTTP requests and responses. Commonly used content headers include but are not limited to:

<ul>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc6266">Content-Disposition</a></code> can indicate that the representation is supposed to be saved as a file, and the proposed file name.
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7231#section-3.1.2.2">Content-Encoding</a></code> indicates compression or encryption algorithms applied to the content.
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7230#section-3.3.2">Content-Length</a></code> indicates the length of the content (in bytes).     
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7231#section-3.1.3.2">Content-Language</a></code> indicates that the body is meant for people literate in some human language(s).     
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7231#section-3.1.4.2">Content-Location</a></code> indicates where the body can be found otherwise (<a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/headers/CommonHeaders.html#must-use-contentlocation-correctly">see below for more details</a>).
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7233#section-4.2">Content-Range</a></code> </a>is used in responses to range requests to indicate which part of the requested resource representation is delivered with the body.
    </li>
    <li>
        <code><a href="https://tools.ietf.org/html/rfc7231#section-3.1.1.5">Content-Type</a></code> </a> indicates the media type of the body content.
    </li>
</ul>

<br>

## <strong><span style="color:red">Must: </span>Use Content-Location Correctly</strong>

<br>

This header is used in the response of either a successful read (GET, HEAD) or successful write operation (PUT, POST or PATCH).

In the case of the GET requests it points to a location where an alternate representation of the entity in the response body can be found. In this case one has to set the Content-Type header as well. For example:

```http
GET /products/123/images HTTP/1.1

HTTP/1.1 200 OK
Content-Type: image/png
Content-Location: /products/123/images?format=raw
```

In the case of mutating HTTP methods, the Content-Location header can be used when there is a response body, and then it indicates that the included response body can be found at the location indicated in the header.

If the header value is the same as the location of the created resource (as indicated by the Location header after POST) or the modified resource (as indicated by the request URI after PUT / PATCH), then the returned body is indeed the current representation of the entity making a subsequent GET operation from the client side not necessary.

If your API returns the new representation after a PUT, PATCH, or POST you should include the Content-Location header to make it explicit, that the returned resource is an up-to-date version.

More details in <a href="https://tools.ietf.org/html/rfc7231#section-3.1.4.2">rfc7231</a>
