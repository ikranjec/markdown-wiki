# Common Data Objects

Definitions of data objects that are good candidates for wider usage:

<br>

## <strong><span style="color:red">Must: </span>Do not expose Stack Traces</strong>

<br>

Stack traces contain implementation details that are not part of an API, and on which clients should never rely. Moreover, stack traces can leak sensitive information that partners and third parties are not allowed to receive and may disclose insights about vulnerabilities to attackers.

<br>

## <strong><span style="color:red">Must: </span>Use common field names</strong>

<br>

There are some data fields that come up again and again in API data. We describe four here:

<ul>
    <li>
        <code>id</code>: the identity of the object. If used, IDs must opaque strings and not numbers. IDs are unique within some documented context, are stable and don't change for a given object once assigned, and are never recycled cross entities.
    </li>
    <li>
        <code>created</code>: when the object was created. If used this must be a date-time construct. 
    </li>
    <li>
        <code>modified</code>: when the object was updated. If used this must be a date-time construct.
    </li>
    <li>
        <code>type</code>: the kind of thing this object is. If used this should be a string. Types allow runtime information on the entity provided that otherwise requires examining the Open API file.
    </li>
</ul>

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Use a Common Money Object</strong>

<br>

Use the following common money structure:

<pre>
    <code>
Money:
  type: object
  properties:
    amount:
      type: number
      format: decimal
      example: 99.95
    currency:
      type: string
      format: iso-4217
      example: EUR
  required:
    - amount
    - currency
    </code>
</pre>

Make sure that you don’t convert the “amount” field to <code>float</code> / <code>double</code> types when implementing this interface in a specific language or when doing calculations. Otherwise, you might lose precision. Instead, use exact formats like Java’s <code><a href="https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html">BigDecimal</a></code>. See <a href="http://stackoverflow.com/a/3730040/342852">Stack Overflow</a> for more info.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Use Common Address Fields</strong>

<br>

Address structures play a role in different functional and use-case contexts, including country variances. The address structure below should be sufficient for most of our business-related use cases. Use it in your APIs — and compatible extend it if necessary for your API concerns:

<pre>
    <code>
address:
    description:
      a common address structure adequate for many use cases
    type: object
    properties:
      salutation:
        type: string
        description: |
          A salutation and/or title which may be used for personal contacts. Hint: not to be confused with the gender information that is stored per customer account
        example: Mr
      first_name:
        type: string
        description: given name(s) or first name(s) of a person; may also include the middle names
        example: Hans Dieter
      last_name:
        type: string
        description: family name(s) or surname(s) of a person
        example: Mustermann
      business_name:
        type: string
        description: company name of the business organization
        example: Consulting Services GmbH
      street:
        type: string
        description: full street address including house number and street name
        example: Schönhauser Allee 103
      additional:
        type: string
        description: further details like suite, apartment number, etc.
        example: 2. Hinterhof rechts
      city:
        type: string
        description: name of the city
        example: Berlin
      zip:
        type: string
        description: Zip code or postal code
        example: 14265
      country_code:
        type: string
        format: iso-3166-1-alpha-2
        example: DE
    required:
      - first_name
      - last_name
      - street
      - city
      - zip
      - country_code        
    </code>
</pre>
