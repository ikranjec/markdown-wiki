# Compatibility

## <strong><span style="color:red">Must: </span> Don’t Break Backward Compatibility</strong>

<br>

Change APIs, but keep all consumers running. Consumers usually have independent release lifecycles, focus on stability, and avoid changes that do not provide additional value. APIs are service contracts that cannot be broken via unilateral decisions.

There are two techniques to change APIs without breaking them:

<ul>
    <li>
        follow rules for compatible extensions
    </li>
    <li>
        introduce new API versions and still support older versions
    </li>
</ul>

We strongly encourage using compatible API extensions and discourage versioning. With Postel’s Law in mind, here are some rules for providers and consumers that allow us to make compatible changes without versioning:

<br>

## <strong><span style="color:red">Must: </span>Do Not Use URI Versioning</strong>

<br>

With URI versioning a (major) version number is included in the path, e.g. /v1/customers. The consumer has to wait until the provider has been released and deployed. If the consumer also supports hypermedia links — even in their APIs — to drive workflows (HATEOAS), this quickly becomes complex. So does coordinating version upgrades — especially with hyperlinked service dependencies — when using URL versioning. To avoid this tighter coupling and complexer release management we do not use URI versioning, and go instead with media type versioning and content negotiation (see above).

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Do Not Use URI Versioning</strong>

<br>

When changing your RESTful APIs, do so in a compatible way and avoid generating additional API versions. Multiple versions can significantly complicate understanding, testing, maintaining, evolving, operating and releasing our systems (<a href="http://martinfowler.com/articles/enterpriseREST.html">supplementary reading</a>).

If changing an API can’t be done in a compatible way, then proceed in one of these three ways:

<ul>
    <li>
        create a new resource (variant) in addition to the old resource variant
    </li>
    <li>
        create a new service endpoint — i.e. a new application with a new API (with a new domain name)
    </li>
    <li>
        create a new API version supported in parallel with the old API by the same microservice
    </li>
</ul>

As we discourage versioning by all means because of the manifold disadvantages, we suggest to only use the first two approaches.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Provide Version Information in OpenAPI Documentation</strong>

<br>

Only the documentation, not the API itself, needs version information.

Example:

```json
"swagger": "2.0",
"info": {
  "title": "Parcel service API",
  "description": "API for <...>",
  "version": "1.0.0",
    <...>
}
```

When versioning your API use the <a href="http://semver.org/">semantic version information</a>
