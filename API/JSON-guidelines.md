# JSON Guidelines

These guidelines provides recommendations for defining JSON data at HelloFresh. JSON here refers to <a href="http://www.rfc-editor.org/rfc/rfc7159.txt">RFC 7159</a> (which updates <a href="https://www.ietf.org/rfc/rfc4627.txt">RFC 4627</a>), the “application/json” media type and custom JSON media types defined for APIs. The guidelines clarifies some specific cases to allow HelloFresh JSON data to have an idiomatic form across teams and services.

<br>

## <strong><span style="color:red">Must: </span>Use Consistent Property Names</strong>

<br>

## <strong><span style="color:red">Must: </span>Property names must be snake_case (and never camelCase).</strong>

<br>

No established industry standard exists, but many popular Internet companies prefer snake_case: e.g. GitHub, Stack Exchange, Twitter. Others, like Google and Amazon, use both - but not only camelCase. It’s essential to establish a consistent look and feel such that JSON looks as if it came from the same hand.

<br>

## <strong><span style="color:red">Must: </span>Property names must be an ASCII subset</strong>

<br>

Property names are restricted to ASCII encoded strings. The first character must be a letter, an underscore or a dollar sign, and subsequent characters can be a letter, an underscore, a dollar sign, or a number.

<br>

## <strong><span style="color:red">Must: </span>Use Consistent Property Values</strong>

<br>

## <strong><span style="color:red">Must: </span>Boolean property values must not be null</strong>

<br>

Schema based JSON properties that are by design booleans must not be presented as nulls. A boolean is essentially a closed enumeration of two values, true and false. If the content has a meaningful null value, strongly prefer to replace the boolean with enumeration of named values or statuses - for example accepted_terms_and_conditions with true or false can be replaced with terms_and_conditions with values yes, no and unknown.

<br>

## <strong><span style="color:red">Must: </span>Date property values should conform to RFC 3399</strong>

<br>

Use the date and time formats defined by RFC 3339:

<ul>
    <li>
        for "date" use strings matching <code>date-fullyear "-" date-month "-" date-mday</code>, for example: <code>2015-05-28</code>
    </li>
    <li>
        for "date-time" use strings matching <code>full-date "T" full-time</code>, for example <code>2015-05-28T14:07:17Z</code>
    </li>
</ul>

Note that the <a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#data-types">OpenAPI format</a> "date-time" corresponds to "date-time" in the RFC) and <code>2015-05-28</code> for a date (note that the OpenAPI format "date" corresponds to "full-date" in the RFC). Both are specific profiles, a subset of the international standard <a href="http://en.wikipedia.org/wiki/ISO_8601">ISO 8601</a>.

A zone offset may be used (both, in request and responses) -- this is simply defined by the standards. However, we encourage restricting dates to UTC and without offsets. For example <code>2015-05-28T14:07:17Z</code> rather than <code>2015-05-28T14:07:17+00:00</code>. From experience we have learned that zone offsets are not easy to understand and often not correctly handled. Note also that zone offsets are different from local times that might be including daylight saving time. Localization of dates should be done by the services that provide user interfaces, if required.

When it comes to storage, all dates should be consistently stored in UTC without a zone offset. Localization should be done locally by the services that provide user interfaces, if required.

Sometimes it can seem data is naturally represented using numerical timestamps, but this can introduce interpretation issues with precision - for example whether to represent a timestamp as 1460062925, 1460062925000 or 1460062925.000. Date strings, though more verbose and requiring more effort to parse, avoid this ambiguity.

<br>

## <strong><span style="color:red">Must: </span>Standards must be used for Language, Country and Currency</strong>

<br>

<ul>
    <li>
        <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1-alpha2 country</a>
        <ul>
            <li>
                (It's "GB", not "UK", even though "UK" has seen some use at HelloFresh)
            </li>
        </ul>
    </li>
    <li>
        <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1 language code</a>
        <ul>
            <li><a href="https://tools.ietf.org/html/bcp47">BCP-47 </a>(based on ISO 639-1) for language variants</li>
        </ul>
    </li>
    <li>
        <a href="http://en.wikipedia.org/wiki/ISO_4217">ISO 4217 currency codes</a>
    </li>
</ul>

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Reserved JavaScript keywords should be avoided</strong>

<br>

Most API content is consumed by non-JavaScript clients today, but for security and sanity reasons, JavaScript (strictly, ECMAScript) keywords are worth avoiding. A list of keywords can be found in the <a href="http://www.ecma-international.org/ecma-262/6.0/#sec-reserved-words">ECMAScript Language Specification</a>.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Array names should be pluralized</strong>

<br>

To indicate they contain multiple values prefer to pluralize array names. This implies that object names should in turn be singular.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Null values should not have their fields removed</strong>

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Empty array values should not be null</strong>

<br>

Empty array values can unambiguously be represented as the the empty list, <code>[]</code>.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Enumerations should be represented as Strings</strong>

<br>

Strings are a reasonable target for values that are by design enumerations.

<br>

## <strong><span style="color:green">Could: </span>Time durations and intervals could conform to ISO 8601</strong>

<br>

Schema based JSON properties that are by design durations and intervals could be strings formatted as recommended by ISO 8601 (<a href="https://tools.ietf.org/html/rfc3339#appendix-A">Appendix A of RFC 3399 contains a grammar for durations</a>).
