# Pagination

## <strong><span style="color:red">Must: </span>Support Pagination</strong>

<br>

Access to lists of data items must support pagination for best client side batch processing and iteration experience. This holds true for all lists that are (potentially) larger than just a few hundred entries.

There are two page iteration techniques:

<ul>
    <li>
        <a href="http://developer.infoconnect.com/paging-results-limit-and-offset">Offset/Limit-based pagination</a>: numeric offset identifies the first page entry
    </li>
    <li>
        <a href="https://dev.twitter.com/overview/api/cursoring">Cursor-based</a> — aka key-based — pagination: a unique key element identifies the first page entry (see also <a href="https://developers.facebook.com/docs/graph-api/using-graph-api/v2.4#paging">Facebook’s guide</a>)
    </li>
</ul>

The technical conception of pagination should also consider user experience related issues. As mentioned in this <a href="https://www.smashingmagazine.com/2016/03/pagination-infinite-scrolling-load-more-buttons/">article</a>, jumping to a specific page is far less used than navigation via next/previous page links. This favours cursor-based over offset-based pagination.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Prefer Cursor-Based Pagination, Avoid Offset-Based Pagination</strong>

<br>

Cursor-based pagination is usually better and more efficient when compared to offset-based pagination. Especially when it comes to high-data volumes and / or storage in NoSQL databases.

Before choosing cursor-based pagination, consider the following trade-offs:

<ul>
    <li>
        Usability/framework support:
        <ul>
            <li>
                Offset / limit based pagination is more known than cursor-based pagination, so it has more framework support and is easier to use for API clients
            </li>
        </ul>
    </li>
    <li>
        Use case: Jump to a certain page
        <ul>
            <li>
                If jumping to a particular page in a range (e.g., 51 of 100) is really a required use case, cursor-based navigation is not feasible
            </li>
        </ul>
    </li>
    <li>
        Variability of data may lead to anomalies in result pages
        <ul>
            <li>
                Offset-based pagination may create duplicates or lead to missing entries if rows are inserted or deleted between two subsequent paging requests.
            </li>
            <li>
                When using cursor-based pagination, paging cannot continue when the cursor entry has been deleted while fetching two pages
            </li>
        </ul>        
    </li>
    <li>
        Performance considerations - efficient server-side processing using offset-based pagination is hardly feasible for:
        <ul>
            <li>
                Higher data list volumes, especially if they do not reside in the database’s main memory
            </li>
            <li>
                Sharded or NoSQL databases
            </li>
        </ul>
    </li>
    <li>
        Cursor-based navigation may not work if you need the total count of results and / or backward iteration support
    </li>
</ul>
