# Data Formats

## <strong><span style="color:red">Must: </span>Use JSON as the Body Payload</strong>

<br>

JSON-encode the body payload. The JSON payload must follow <a href="https://tools.ietf.org/html/rfc7159">RFC-7159</a> by having (if possible) a serialized object as the top-level structure, since it would allow for future extension. This also applies for collection resources where one naturally would assume an array.

<br>

## <strong><span style="color:red">Must: </span>Use Standard Date and Time Formats</strong>

<br>

Read more about date and time format in <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/json-guidelines/JsonGuidelines.html#date-property-values-should-conform-to-rfc-3399">Json Guideline</a>.

<br>

## <strong><span style="color:red">Must: </span>Use Standards for Country, Language and Currency Codes</strong>

Use the following standard formats for country, language and currency codes:

<ul>
    <li>
        <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1-alpha2 country codes</a>
        <ul>
            <li>
                (It is “GB”, not “UK”, even though “UK” has seen some use at HelloFresh)
            </li>
        </ul>
    </li>
    <li>
        <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1 language code</a>
        <ul>
            <li>
                <a href="https://tools.ietf.org/html/bcp47">BCP-47</a> (based on ISO 639-1) for language variants
            </li>
        </ul>
    </li>
    <li>
        <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217 currency codes</a>
    </li>
</ul>

<br>

## <strong><span style="color:green">Could: </span>Use other Media Types than JSON</strong>

<br>

If for given use case JSON does not make sense, for instance when providing attachments in form of PDFs, you should use another, more sufficient media type. But only do this if you can not transfer the information in JSON.

## <strong>HTTP headers</strong>

Http headers including the proprietary headers. Use the <a href="http://tools.ietf.org/html/rfc7231#section-7.1.1.1">HTTP date format defined in RFC 7231</a>.
