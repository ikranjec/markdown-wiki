# Resources

## <strong><span style="color:red">Must: </span>Avoid Actions — Think About Resources</strong>

<br>

REST is all about your resources, so consider the domain entities that take part in web service interaction, and aim to model your API around these using the standard HTTP methods as operation indicators. For instance, if an application has to lock articles explicitly so that only one user may edit them, create an article lock with PUT or POST instead of using a lock action.

Request:

```http
PUT /recipes/{recipe-id}
```

The added benefit is that you already have a service for browsing and filtering article locks.

<br>

## <strong><span style="color:red">Must: </span>Keep URLs Verb-Free</strong>

<br>

The API describes resources, so the only place where actions should appear is in the HTTP methods. In URLs, use only nouns.

<br>

## <strong><span style="color:red">Must: </span>Use Domain-Specific Resource Names</strong>

<br>

API resources represent elements of the application’s domain model. Using domain-specific nomenclature for resource names helps developers to understand the functionality and basic semantics of your resources. It also reduces the need for further documentation outside the API definition. For example, "purchase-order-items" is superior to "purchase-items" in that it clearly indicates which business object it represents. Along these lines, “items” is too general.

<br>

## <strong><span style="color:red">Must: </span>Identify resources and Sub-Resources via Path Segments</strong>

<br>

Basic URL structure:

```http
/{resources}/[resource-id]/{sub-resources}/[sub-resource-id]
```

Examples:

```http
/orders/1681e6b88ec1/items
/orders/1681e6b88ec1/items/1
```

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Define <em>useful</em> resources</strong>

<br>

As a rule of thumb resources should be defined to cover 90% of all its client's use cases. A useful resource should contain as much information as necessary, but as little as possible. A great way to support the last 10% is to allow clients to specify their needs for more/less information by supporting filtering and <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/hyper-media/Hypermedia.md#should-allow-embedding-of-complex-subresources">embedding</a>.

<br>

## <strong><span style="color:green">Could: </span>Consider Using (Non-) Nested URLs</strong>

<br>

If a sub-resource is only accessible via its parent resource and may not exists without parent resource, consider using a nested URL structure, for instance:

```http
/orders/1681e6b88ec1/items/1
```

However, if the resource can be accessed directly via its unique id, then the API should expose it as a top-level resource. For example, customer is a collection for sales orders; however, sales orders have globally unique id and some services may choose to access the orders directly, for instance:

```http
/customers/1681e6b88ec1
/orders/5273gh3k525a
```

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Limit number of Resources</strong>

<br>

To keep maintenance and service evolution manageable, we should follow "functional segmentation" and "separation of concern" design principles and do not mix different business functionalities in same API definition. In this sense the number of resources exposed via API should be limited - our experience is that a typical range of resources for a well-designed API is between 4 and 8. There may be exceptions with more complex business domains that require more resources, but you should first check if you can split them into separate subdomains with distinct APIs.

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Limit number of Sub-Resource Levels</strong>

<br>

There are main resources (with root url paths) and sub-resources (or “nested” resources with non-root urls paths). Use sub-resources if their life cycle is (loosely) coupled to the main resource, i.e. the main resource works as collection resource of the subresource entities. You should use <= 3 sub-resource (nesting) levels -- more levels increase API complexity and url path length. (Remember, some popular web browsers do not support URLs of more than 2000 characters)
