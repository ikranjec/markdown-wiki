# HTTP

## <strong><span style="color:red">Must: </span>Use HTTP Methods Correctly</strong>

<br>

Be compliant with the standardized HTTP method semantics summarized as follows:

## <strong>GET</strong>

<br>

GET requests are used to read a single resource or query set of resources.

<ul>
    <li>
        GET requests for individual resources will usually generate a 404 if the resource does not exist
    </li>
    <li>
        GET requests for collection resources may return either 200 (if the listing is empty) or 404 (if the list is missing)
    </li>
    <li>
        GET requests must NOT have request body payload
    </li>
</ul>

<b>Note: </b>GET requests on collection resources should provide a sufficient filter mechanism as well as <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/pagination/Pagination.html">pagination</a>.

<br>

## <strong>POST</strong>

<br>

POST requests are idiomatically used to create single resources on a collection resource endpoint, but other semantics on single resources endpoint are equally possible. The semantic for collection endpoints is best described as »<em>please add the enclosed representation to the collection resource identified by the URL«. The semantic for single resource endpoints is best described as »please execute the given well specified request on the collection resource identified by the URL</em>«.

<ul>
    <li>
        POST request should only be applied to collection resources, and normally not on single resource, as this has an undefined semantic
    </li>
    <li>
        on successful POST requests, the server will create one or multiple new resources and provide their URI/URLs in the response
    </li>
    <li>
        successful POST requests will usually generate 200 (if resources have been updated), 201 (if resources have been created), and 202 (if the request was accepted but has not been finished yet)
    </li>
</ul>

<b>More generally: </b>POST should be used for scenarios that cannot be covered by the other methods sufficiently. For instance, GET with complex (e.g. SQL like structured) query that needs to be passed as request body payload because of the URL-length constraint. In such cases, make sure to document the fact that POST is used as a workaround.

<b>Note: </b>Resource IDs with respect to POST requests are created and maintained by server and returned with response payload. Posting the same resource twice is by itself not required to be idempotent and may result in multiple resource instances. Anyhow, if external URIs are present that can be used to identify duplicate requests, it is best practice to implement POST in an idempotent way.

<br>

## <strong>PUT</strong>

<br>

PUT requests are used to update single resources or an entire collection resources. The semantic is best described as »<em>please put the enclosed representation at the resource mentioned by the URL</em>«.

<ul>
    <li>
        PUT requests are usually applied to single resources, and not to collection resources, as this would imply replacing the entire collection
    </li>
    <li>
        PUT requests are usually robust against non-existence of resources by implicitly creating before updating
    </li>
    <li>
        on successful PUT requests, the server will replace the entire resource addressed by the URL with the representation passed in the payload
    </li>
    <li>
        successful PUT requests will usually generate 200 or 204 (if the resource was updated - with or without actual content returned), and 201 (if the resource was created)
    </l>
</ul>

<b>Note: </b> Resource IDs with respect to PUT requests are maintained by the client and passed as a URL path segment. Putting the same resource twice is required to be idempotent and to result in the same single resource instance. If PUT is applied for creating a resource, only URIs should be allowed as resource IDs. If URIs are not available POST should be preferred.

<br>

## <strong>PATCH</strong>

<br>

PATCH request are only used for partial update of single resources, i.e. where only a specific subset of resource fields should be replaced. The semantic is best described as »please change the resource identified by the URL according to my change request«. The semantic of the change request is not defined in the HTTP standard and must be described in the API specification by using suitable media types.

<ul>
    <li>
        PATCH requests are usually applied to single resources, and not on collection resources, as this would imply patching on the entire collection
    </li>
    <li>
        PATCH requests are usually not robust against non-existence of resource instances
    </li>
    <li>
        on successful PATCH requests, the server will update parts of the resource addressed by the URL as defined by the change request in the payload
    </li>
    <li>
        successful PATCH requests will usually generate 200 or 204 (if resources have been updated
        <ul>
            <li>
                with or without updated content returned)
            </li>
        </ul>
    </li>
</ul>

<b>Note: </b>since implementing PATCH correctly is a bit tricky, we strongly suggest to choose one and only one of the following patterns per endpoint, unless forced by a <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/compatibility/Compatibility#compatibility">backwards compatible change</a>. In preference order:

<ol>
    <li>
        use PUT with complete objects to update a resource as long as feasible (i.e. do not use PATCH at all).
    </li>
    <li>
        use PATCH with partial objects to only update parts of a resource, when ever possible. (This is basically <a href="https://tools.ietf.org/html/rfc7396">JSON Merge Patch</a>, a specialized media type <code>application/merge-patch+json</code> that is a partial resource representation.)
    </li>
    <li>
        use PATCH with <a href="http://tools.ietf.org/html/rfc6902">JSON Patch</a>, a specialized media type <code>application/json-patch+json</code> that includes instructions on how to change the resource.
    </li>
    <li>
        use POST (with a proper description of what is happening) instead of PATCH if the request does not modify the resource in a way defined by the semantics of the media type.
    </li>
</ol>

In practice <a href="https://tools.ietf.org/html/rfc7396">JSON Merge Patch</a> quickly turns out to be too limited, especially when trying to update single objects in large collections (as part of the resource). In this cases <a href="http://tools.ietf.org/html/rfc6902">JSON Patch</a> can shown its full power while still showing readable patch requests (<a href="http://erosb.github.io/post/json-patch-vs-merge-patch">see also</a>).

<br>

## <strong>DELETE</strong>

<br>

DELETE request are used to delete resources. The semantic is best described as »<em>please delete the resource identified by the URL</em>«.

<ul>
    <li>
        DELETE requests are usually applied to single resources, not on collection resources, as this would imply deleting the entire collection
    </li>
    <li>
        successful DELETE request will usually generate 200 (if the deleted resource is returned) or 204 (if no content is returned)
    </li>
    <li>
        failed DELETE request will usually generate 404 (if the resource cannot be found) or 410 (if the resource was already deleted before)
    </li>
</ul>

<br>

## <strong>HEAD</strong>

<br>

HEAD requests are used retrieve to header information of single resources and resource collections.

<ul>
    <li>
        HEAD requests are used retrieve to header information of single resources and resource collections.
    </li>
</ul>

<br>

## <strong>OPTIONS</strong>

<br>

OPTIONS are used to inspect the available operations (HTTP methods) of a given endpoint.

<ul>
    <li>
        OPTIONS requests usually either return a comma separated list of methods (provided by an <code>Allow:</code>-Header) or as a structured list of link templates
    </li>
</ul>

<b>Note: </b>OPTIONS is rarely implemented, though it could be used to self-describe the full functionality of a resource.

<br>

## <strong><span style="color:red">Must: </span>Fulfill Safeness and Idempotency Properties</strong>

<br>

An operation can be...

<ul>
    <li>
        idempotent, i.e. operation will produce the same results if executed once or multiple times (note: this does not necessarily mean returning the same status code)
    </li>
    <li>
        safe, i.e. must not have side effects such as state changes
    </li>
</ul>

Method implementations must fulfill the following basic properties:
| HTTP method | safe | idempotent |
| :-------------------------- | :----------------------- | ------------------------: |
| OPTIONS | Yes | YES |
| HEAD | Yes | YES |
| GET | Yes | YES |
| PUT | No | YES |
| POST | No | NO |
| DELETE | No | YES |
| PATCH | No | NO |

<br>

## <strong><span style="color:red">Must: </span>Use Meaningful HTTP Status Codes</strong>

<br>

## <strong>Success Codes</strong>

<table>
   <thead>
      <tr>
         <th>Code</th>
         <th>Meaning</th>
         <th>Methods</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>200</td>
         <td>OK - this is the standard success response</td>
         <td>All</td>
      </tr>
      <tr>
         <td>201</td>
         <td>Created - Returned on successful entity creation. You are free to return either an empty response or the created resource in conjunction with the Content-Location header. (More details found in the <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/headers/CommonHeaders.html">Common Headers section</a>.) <em>Always</em> set the Location header.</td>
         <td>POST, PUT</td>
      </tr>
      <tr>
         <td>202</td>
         <td>Accepted - The request was successful and will be processed asynchronously.</td>
         <td>POST, PUT, DELETE, PATCH</td>
      </tr>
      <tr>
         <td>204</td>
         <td>No content - There is no response body</td>
         <td>PUT, DELETE</td>
      </tr>
   </tbody>
</table>

<br>

## <strong>Redirection Codes</strong>

<br>

<table>
   <thead>
      <tr>
         <th>Code</th>
         <th>Meaning</th>
         <th>Methods</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>301</td>
         <td>Moved Permanently - This and all future requests should be directed to the given URI.</td>
         <td>All</td>
      </tr>
      <tr>
         <td>303</td>
         <td>See Other - The response to the request can be found under another URI using a GET method.</td>
         <td>PATCH, POST, PUT, DELETE</td>
      </tr>
      <tr>
         <td>304</td>
         <td>Not Modified - resource has not been modified since the date or version passed via request headers If-Modified-Since or If-None-Match.</td>
         <td>GET</td>
      </tr>
   </tbody>
</table>

<br>

## <strong>Client Side Error Codes</strong>

<br>

<table>
   <thead>
      <tr>
         <th>Code</th>
         <th>Meaning</th>
         <th>Methods</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>400</td>
         <td>Bad request - generic / unknown error</td>
         <td>All</td>
      </tr>
      <tr>
         <td>401</td>
         <td>Unauthorized - the users must log in (this often means “Unauthenticated”)</td>
         <td>All</td>
      </tr>
      <tr>
         <td>403</td>
         <td>Forbidden - the user is not authorized to use this resource</td>
         <td>All</td>
      </tr>
      <tr>
         <td>404</td>
         <td>Not found - the resource is not found</td>
         <td>All</td>
      </tr>
      <tr>
         <td>405</td>
         <td>Method Not Allowed - the method is not supported, see OPTIONS</td>
         <td>All</td>
      </tr>
      <tr>
         <td>406</td>
         <td>Not Acceptable - resource can only generate content not acceptable according to the Accept headers sent in the request</td>
         <td>All</td>
      </tr>
      <tr>
         <td>408</td>
         <td>Request timeout - the server times out waiting for the resource</td>
         <td>All</td>
      </tr>
      <tr>
         <td>409</td>
         <td>Conflict - request cannot be completed due to conflict, e.g. when two clients try to create the same resource or if there are concurrent, conflicting updates</td>
         <td>PUT, DELETE, PATCH</td>
      </tr>
      <tr>
         <td>410</td>
         <td>Gone - resource does not exist any longer, e.g. when accessing a resource that has intentionally been deleted</td>
         <td>All</td>
      </tr>
      <tr>
         <td>412</td>
         <td>Precondition Failed - returned for conditional requests, e.g. If-Match if the condition failed. Used for optimistic locking.</td>
         <td>PUT, DELETE, PATCH</td>
      </tr>
      <tr>
         <td>415</td>
         <td>Unsupported Media Type - e.g. clients sends request body without content type</td>
         <td>PUT, DELETE, PATCH</td>
      </tr>
      <tr>
         <td>423</td>
         <td>Locked - Pessimistic locking, e.g. processing states</td>
         <td>PUT, DELETE, PATCH</td>
      </tr>
      <tr>
         <td>428</td>
         <td>Precondition Required - server requires the request to be conditional (e.g. to make sure that the “lost update problem” is avoided).</td>
         <td>All</td>
      </tr>
      <tr>
         <td>429</td>
         <td>Too many requests - the client does not consider rate limiting and sent too many requests. See <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/http/Http.html#must-use-429-with-headers-for-rate-limits">"Use 429 with Headers for Rate Limits"</a>.</td>
         <td>All</td>
      </tr>
   </tbody>
</table>

<br>

## <strong>Server Side Error Codes</strong>

<br>

<table>
   <thead>
      <tr>
         <th>Code</th>
         <th>Meaning</th>
         <th>Methods</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>500</td>
         <td>Internal Server Error - a generic error indication for an unexpected server execution problem (here, client retry may be senseful)</td>
         <td>All</td>
      </tr>
      <tr>
         <td>501</td>
         <td>Not Implemented -  server cannot fulfill the request (usually implies future availability, e.g. new feature).</td>
         <td>All</td>
      </tr>
      <tr>
         <td>503</td>
         <td>Service Unavailable - server is (temporarily) not available (e.g. due to overload) -- client retry may be senseful.</td>
         <td>All</td>
      </tr>
   </tbody>
</table>

All error codes can be found in <a href="https://tools.ietf.org/html/rfc7231#section-6">RFC7231</a> and <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">Wikipedia</a> or via <a href="https://httpstatuses.com/">https://httpstatuses.com/</a>.

<br>

## <strong><span style="color:red">Must: </span>Provide Error Documentation</strong>

<br>

APIs should define the functional, business view and abstract from implementation aspects. Errors become a key element providing context and visibility into how to use an API. The error object should be extended by an application-specific error identifier if and only if the HTTP status code is not specific enough to convey the domain-specific error semantic. For this reason, we use a standardized error return object definition — see <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/common-data-objects/CommonDataObjects.html#must-use-common-error-return-objects"><em>Use Common Error Return Objects</em></a>.

The OpenAPI specification shall include definitions for error descriptions that will be returned; they are part of the interface definition and provide important information for service clients to handle exceptional situations and support troubleshooting. You should also think about a troubleshooting board — it is part of the associated online API documentation, provides information and handling guidance on application-specific errors and is referenced via links of the API definition. This can reduce service support tasks and contribute to service client and provider performance.

Service providers should differentiate between technical and functional errors. In most cases it's not useful to document technical errors that are not in control of the service provider unless the status code convey application-specific semantics. The list of status code that can be omitted from API specifications includes but is not limited to:

<ul>
    <li>
        <code>401 Unauthorized</code>
    </li>
    <li>
        <code>403 Forbidden</code>
    </li>
    <li>
        <code>404 Not Found</code> unless it has some additional semantics
    </li>
    <li>
        <code>405 Method Not Allowed</code>
    </li>
    <li>
        <code>406 Not Acceptable</code>
    </li>
    <li>
        <code>408 Request Timeout</code>
    </li>
    <li>
        <code>413 Payload Too Large</code>
    </li>
    <li>
        <code>414 URI Too Long</code>
    </li>
    <li>
        <code>415 Unsupported Media Type</code>
    </li>
    <li>
        <code>500 Internal Server Error</code>
    </li>
    <li>
        <code>502 Bad Gateway</code>
    </li>
    <li>
        <code>503 Service Unavailable</code>
    </li>
    <li>
        <code>504 Gateway Timeout</code>
    </li>
</ul>

Even though they might not be documented - they may very much occur in production, so clients should be prepared for unexpected response codes, and in case of doubt handle them like they would handle the corresponding x00 code. Adding new response codes (specially error responses) should be considered a compatible API evolution.

Functional errors on the other hand, that convey domain-specific semantics, must be documented and are strongly encouraged to be expressed with <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/common-data-objects/CommonDataObjects.html#must-use-common-error-return-objects"><em>Problem types</em></a>.

<br>

## <strong><span style="color:red">Must: </span>Use 429 with Headers for Rate Limits</strong>

<br>

<b>Note: </b>This is already handled by the api gateway, which your service will be behind. It's important to remeber to configure properly the rate limit for your service.

APIs that wish to manage the request rate of clients must use the <a href="http://tools.ietf.org/html/rfc6585">'429 Too Many Requests'</a> response code if the client exceeded the request rate and therefore the request can't be fulfilled. Such responses must also contain header information providing further details to the client. There are two approaches a service can take for header information:

<ul>
    <li>
        Return a <a href="https://tools.ietf.org/html/rfc7231#section-7.1.3">'Retry-After'</a> header indicating how long the client ought to wait before making a follow-up request. The Retry-After header can contain a HTTP date value to retry after or the number of seconds to delay. Either is acceptable but APIs should prefer to use a delay in seconds.
    </li>
    <li>
        Return a trio of 'X-RateLimit' headers. These headers (described below) allow a server to express a service level in the form of a number of allowing requests within a given window of time and when the window is reset.
    </li>
</ul>

The 'X-RateLimit' headers are:

<ul>
    <li>
        <code>X-RateLimit-Limit</code>: The maximum number of requests that the client is allowed to make in this
    </li>
    <li>
        <code>X-RateLimit-Remaining</code>: The number of requests allowed in the current window.
    </li>
    <li>
        <code>X-RateLimit-Reset</code>: The relative time in seconds when the rate limit window will be reset.
    </li>
</ul>

The reason to allow both approaches is that APIs can have different needs. Retry-After is often sufficient for general load handling and request throttling scenarios and notably, does not strictly require the concept of a calling entity such as a tenant or named account. In turn this allows resource owners to minimise the amount of state they have to carry with respect to client requests. The 'X-RateLimit' headers are suitable for scenarios where clients are associated with pre-existing account or tenancy structures. 'X-RateLimit' headers are generally returned on every request and not just on a 429, which implies the service implementing the API is carrying sufficient state to track the number of requests made within a given window for each named entity.
