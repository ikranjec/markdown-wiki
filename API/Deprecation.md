# Deprecation

Sometimes it is necessary to phase out an API endpoint (or version). I.e. this may be necessary if a field is no longer supported in the result or a whole business functionality behind an endpoint has to be shut down. There are many other reasons as well.

<br>

## <strong><span style="color:red">Must: </span>Obtain Approval of Clients</strong>

<br>

Before shutting down an API (or version of an API) the producer must make sure, that all clients have given their consent to shut down the endpoint. Producers should help consumers to migrate to a potential new endpoint (i.e. by providing a migration manual). After all clients are migrated, the producer may shut down the deprecated API.

<br>

## <strong><span style="color:red">Must: </span>Not Start Using Deprecated APIs</strong>

<br>

Clients must not start using deprecated parts of an API.

<br>

## <strong><span style="color:red">Must: </span>Reflect Deprecation in API Definition</strong>

<br>

API deprecation must be part of the OpenAPI definition. If a method on a path, a whole path or even a whole API endpoint (multiple paths) should be deprecated, the producers must set <code>deprecated=true</code> on each method / path element that will be deprecated (OpenAPI 2.0 only allows you to define deprecation on this level). If deprecation should happen on a more fine grained level (i.e. query parameter, payload etc.), the producer should set <code>deprecated=true</code> on the affected method / path element and add further explanation to the <code>description</code> section.

If <code>deprecated</code> is set to <code>true</code>, the producer must describe what clients should use instead and when the API will be shut down in the <code>description</code> section of the API definition.

## <strong><span style="color:darkgoldenrod">Should: </span>Add a Warning Header to Responses</strong>

<BR>

During deprecation phase, the producer should add a <code>Warning</code> header (see <a href="https://tools.ietf.org/html/rfc7234#section-5.5">RFC 7234 - Warning header</a>) field. When adding the <code>Warning</code> header, the <code>warn-code</code> must be <code>299</code> and the <code>warn-text</code> should be in form of <em>"The path/operation/parameter/... {name} is deprecated and will be removed by {date}. Please see {link} for details."</em> with a link to a documentation describing why the API is no longer supported in the current form and what clients should do about it. Adding the <code>Warning</code> header is not sufficient to gain client consent to shut down an API.
