# API Naming

## <strong><span style="color:red">Must: </span>Use lowercase separate words with hyphens for Path Segments</strong>

<br>

Example:

<pre>
<code>/purchase-orders/{purchase-order-id}</code>
</pre>

This applies to concrete path segments and not the names of path parameters. For example <code>{purchase_order_id}</code> would be ok as a path parameter.

<br>

## <strong><span style="color:red">Must: </span>Use snake_case (never camelCase) for Query Parameters</strong>

<br>

Examples:

<pre>
<code>customer_number, order_id, billing_address</code>
</pre>

<br>

## <strong><span style="color:red">Must: </span>Use Hyphenated HTTP Headers</strong>

<br>

## <strong><span style="color:red">Must: </span>Pluralize Resource Names</strong>

<br>

Usually, a collection of resource instances is provided (at least API should be ready here). The special case of a resource singleton is a collection with cardinality 1.

<br>

## <strong><span style="color:red">Must: </span>Avoid Trailing Slashes</strong>

<br>

The trailing slash must not have specific semantics. Resource paths must deliver the same results whether they have the trailing slash or not.

<br>

## <strong><span style="color:red">Must: </span>Use Conventional Query Strings</strong>

<br>

If you provide query support for sorting, pagination, filtering functions or other actions, use the following standardized naming conventions:

<ul>
    <li>
        <code>q</code> — default query parameter (e.g. used by browser tab completion); should have an entity specific alias, like sku
    </li>
    <li>
        <code>limit</code> — to restrict the number of entries. See Pagination section below. Hint: You can use size as an alternate query string.
    </li>
    <li>
        <code>cursor</code> — key-based page start. See Pagination section below.
    </li>
    <li>
        <code>offset</code> — numeric offset page start. See Pagination section below. Hint: In combination with limit, you can use page as an alternative to offset.
    </li>
    <li>
        <code>sort</code> — comma-separated list of fields to sort. To indicate sorting direction, fields my prefixed with + (ascending) or - (descending, default), e.g. /sales-orders?sort=+id
    </li>
    <li>
        <code>fields</code> — to retrieve a subset of fields. See <a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/performance/Performance.md#should-support-filtering-of-resource-fields"><em>Support Filtering of Resource Fields</em></a> below.
    </li>
    <li>
        <code>embed</code> — to expand embedded entities (ie.: inside of an article entity, expand silhouette code into the silhouette object). Implementing “expand” correctly is difficult, so do it with care. See <em><a href="https://hellofresh.gitbooks.io/microservice-guidelines/content/api/hyper-media/Hypermedia.md#should-allow-embedding-of-complex-subresources">Embedding resources</a></em> for more details.
    </li>    
</ul>

<br>

## <strong><span style="color:darkgoldenrod">Should: </span>Prefer Hyphenated-Pascal-Case for HTTP header Fields</strong>

<br>

This is for consistency in your documentation (most other headers follow this convention). Avoid camelCase (without hyphens). Exceptions are common abbreviations like “ID.”

Examples:

```http
Accept-Encoding
Apply-To-Redirect-Ref
Disposition-Notification-Options
Original-Message-ID
```

See also: <a href="http://tools.ietf.org/html/rfc7230#page-22">HTTP Headers are case-insensitive (RFC 7230)</a>.

<br>

## <strong><span style="color:green">Could: </span>Use Standardized Headers</strong>

<br>

Use <a href="http://en.wikipedia.org/wiki/List_of_HTTP_header_fields">this list</a> and mention its support in your OpenAPI definition.
